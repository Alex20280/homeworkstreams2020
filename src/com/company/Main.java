package com.company;

import java.io.*;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        System.out.println("Input filepath and file name to read it");
        Scanner scanner = new Scanner(System.in);
        String scan = scanner.nextLine();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(scan))) {
            String s = "";
            while (true) {
                try {
                    if ((s = bufferedReader.readLine()) == null) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("File content: " + s);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

